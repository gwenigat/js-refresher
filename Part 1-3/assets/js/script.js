/* JS REFRESHER (Pt. 1) */

	/*
	This page covers the ff:
		- Variables
		- Using the console.log
		- Ways to write JS
		- Data Types
		- Objects
		- Arithmetic Operators
		- Assignment Operators
		- Type Conversion/Coercion
		- Comparison Operators with If-Else Statements
		- Logical Operators with If-Else Statements
		- Conditional Ternary Operator

	Folder Sources: wd004-s1-d1, s2-d1
	*/


/* VARIABLES
	- Variables are containers for data. A variable is a named location for storing a value.

	Variable Names:
		- can contain letters, numbers, underscores or dollar signs.
		- must begin with a letter and are case-sensitive.
		- as best practice, start variable names with a lowercase letter & use camelCase for multiple words.
		- should be indicative or descriptive of the value being stored to avoid confusion.
		- take note of reserved keywords in JS. They cannot be used as a variable name.

	Variable Syntax:
		var <variable name> = <value/data> / ex: let name = gwen

	Variable Declaration & Initialization
		var one = 1 	- variable stores numeric value
		var two = 'two' - variable stores string value
		var three;  	- declared a variable without assigning a value
	
	Variable Types:
		- var
		- let
			can both receive an error of "identifier (variable name) has already been declared" if the variable name is used again
		- const (or constant variable, can't be defined again or override, can't be assigned to another value)
*/

	// Multiple Variable Declarations:

		let productName2 = "shampoo", productID2 = "S-124"
		console.log(productName2, productID2) // returns: shampoo S-124

/* Variable Errors */

	/*
	Common Error 1

		console.log(productNme)

		- The above code will result in: "ReferenceError: productNme is not defined." It means that the variable that we specified
		was not given a value or does exist. Be careful with spelling in JS.

	Common Error 2

		let class = "Batch 58"
		console.log(class) // returns error: Uncaught SyntaxError: Unexpected token 'class'
	*/


/* USING CONSOLE.LOG
	- The console.log () method writes a message to the console.
	- Useful for testing purposes.
	- Tells the computer to print specified message on the console.
*/
	
	let numeric = 25
	console.log(numeric) // returns: 25


/* WAYS TO WRITE JS
	- Concatenate / ex: console.log("My name is " + name)
	- Template Literal (ES6) / ex: console.log(`My name is ${name}`)
	Both will return as: My name is Gwen
*/


/* DATA TYPES (has 2 categories)
	1. Value Types or Primitives:
		- string, number, boolean, undefined, null, symbol (new to ES6)
	
	2. Non-Primitive, Reference Types or Objects:
		- object, function, array (both array & function are objects, too)
*/

/* DATA TYPE: STRING
	- it includes alphanumeric characters and special characters which have to be enclosed in either single or double quotes.
		ex. let name = "Cate Blanchett"
	
	String has a .length property that counts how many characters were used including the space
		- console.log(name.length) - returns as: 14
		- console.log(typeof name) - to know what type of data is the variable
*/

/* DATA TYPE: NUMBER
	- includes all possible numeric values including NaN (Not a Number), positive infinity and negative infinity.
	- we use numbers/numeric data type for computations.
	- do not enclose with a quote or a double quote.
		
	ex.
	let number1 = 5
	let number2 = 6

	console.log(number1 + number2) - returns as: 11
		- JS will properly compute the numbers if the data types are correct.

	NaN: this is generated when a Math function fails or when trying to parse a number fails. It's the only value in JS that doesn't Equal itself.
	The surest way to determine if something in JS is NaN is to compare it against itself. If it returns false, it is NaN.
*/

/* DATA TYPE: BOOLEAN
	- holds true or false
	
	ex.
	let isHappy = true
	let isSad = false

	console.log(isHappy) - returns as: true
*/

/* DATA TYPE: UNDEFINED
	- represents the state of a variable that's been declared but has no assigned value.

	ex.
	let firstResponder
	console.log(firstResponder) - returns as: undefined
*/

/* DATA TYPE: NULL
	- is used to intentionally expressed the absence of a value in a variable declaration. It is a value.

	ex.
	let y = null;
	console.log(y) - returns as: null
*/


/* OBJECTS
	- Objects are variables, too, but they can contain many values.
	- The values are written as:

	name: value
		- name and value are separated by a colon
		- name is a.k.a "key"
		- value is a.k.a "property"

	ex.
	let person = {
		name: "Jake Doe",
		age: 30,
		isEmployed: true
	}
		console.log(person.name) - returns as: Jake Doe
		console.log(person.isEmployed) - returns as: true
		console.log(person) - returns as: {name: "Jake Doe", age: 30, isEmployed: true}

	When creating an object, make sure that each property is separated by a comma.
*/


/* ARITHMETIC OPERATORS (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Arithmetic)
	- used to compute in JS
	- Mathematical operations are carried out using simple operators:
		+ for addition
		- for subtraction
		* for multiplication
		/ for division
		% (modulo) for remainder
		() for overriding the default operational priority (MDAS)
	- Arithmetic operators only compute correctly when using the appropriate data type (numbers/numeric)
*/

	// ex. 1
	let price = 500.50
	let quantity = 2
	let product = price * quantity

		console.log(product)
		console.log(price*quantity)
		// both will return as: 1001

	// ex. 2
	let mdas = 3+4*5
		console.log(mdas) // returns as: 23 (because JS follows the MDAS flow)

	// ex. 3 (overriding MDAS)
	let pmdas = (3+4)*5
		console.log(pmdas) // returns as: 35
	
	// ex. 4
	let quotient = 20/5
		console.log(quotient) // returns as: 4

	// ex. 5
	let quotient2 = 7/3
		console.log(quotient2) // returns as: 2.33

	// ex. 6 (Modulo)
	let remainder = 22%7
		console.log(remainder) // returns as: 1

/* INCREMENT OPERATOR (++)
	- increment operator adds one to the operand and returns a value
*/
	
	let number5 = 10
	let increment = ++number5
		console.log(increment) // returns as: 11

/* DECREMENT OPERATOR (--)
	- subtracts one to the operand and returns a value
*/

	let number6 = 11
	let decrement = --number6
		console.log(decrement) // returns as: 10

	/* PREFIX & POSTFIX INCREMENT & DECREMENT */

	let prefix = 6
		console.log(++prefix) // result is 7
		console.log(prefix) // result is 7

	let postfix = 5
		console.log(postfix++) // returns the value before incrementation (which is 5)
		console.log(postfix) // added 1, so the result is 6
	// The code is read left to right, therefore, the returning variable before incrementing


/* ASSIGNMENT OPERATORS (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Assignment)
	1. Basic Assignment Operator (=)
		- adds the value of the right operand to a variable and assigns the result to the variable.
*/

	// ex.
	let numberEx1 = 15
	let numberEx2 = 5
	let sum = numberEx1 + numberEx2
		console.log (sum) // returns as: 20
	sum = numberEx2
		console.log(sum) // returns as: 5
	
/*  2. Addition Assignment Operator (+=)
		- adds the value of the right operand to a variable and assigns the result to the variable.
*/

	sum += 5
		console.log(sum) // returns as: 10

/*  3. Subtraction Assignment Operator (-=)
		- subtracts the value of the right operand to a variable and assigns the result to the variable.
*/

	sum -= 3
		console.log(sum) // returns as: 7

/* Basic Addition and Subtraction Assignment Operator in the console.log */

	let newPrice = 35;
	console.log(newPrice = 36)
	console.log(newPrice) // result is 36

	let newPrice2 = 250
	console.log(newPrice2 += 50)
	console.log(newPrice2) // result is 300

	let newPrice3 = 100
	console.log(newPrice3 -=25)
	console.log(newPrice3) // result is 75


/*  4-6. Multiplication, Division, & Modulo Assignment Operators (*=, /=, %=) */

	let newProduct = 5
	console.log(newProduct *= 25) // results to 125
	console.log(newProduct) // the value is saved and is now 125

	let newQuotient = 10
	console.log(newQuotient /= 5) // results to 2
	console.log(newQuotient) // the value is saved and is now 2

	let newMod = 100
	console.log(newMod %= 10) // results to 0
	console.log(newMod) // the value us saved and is now 0

/* Activity */

	let first = 25
		console.log(first) // 25

	let second = 5
		console.log(second) // 5
		console.log(first+5) // 30

	second += 5
		console.log(second) // 10

	let difference = first - second
		console.log(difference) // 15

	let var1 = first * second
		console.log(var1) // 250

	let quotient3 = first / second
		console.log(quotient3) // 2.5

	let modulo = first % second
		console.log(modulo) // 5


/* TYPE CONVERSION/COERCION
	- When JS is given an operation with conflicting data types, it tries to normalize them first before performing the operation.
	- This is why numbers in a mathematical operation are converted to strings when a string is included in the operation.
	- Adding a number to a boolean will convert the boolean to either 0(false) or 1(true) before performing the operation.
	
	ex.
	false + 2 will result to 2
	true + 2 will result to 3
*/


/* COMPARISON OPERATORS (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Comparison)
	 - Used to compare 2 values and return either true or false. JS has both strict and type-converting comparisons.
	 - A strict comparison (===) is only true if the operands are of the same type and the contents match.
	 - The more commonly used (==) is an abstract comparison, which converts the operands to the same type before making a comparison.
*/

/*	1. Equality Operator (==)
		- Returns "true" if both values are equal. If values compared are of different types, JS converts them and
	then applies a strict comparison.	
*/

	console.log("Equality Operator (==)")
	console.log(1 == 1) // results to "true"
	console.log("1" == 1) // results to "true"

	let ex1 = "1"
	let ex2 = "2"
		console.log(ex1 == ex2) // results to "false", data types are the same but values are different
		console.log(1 == true) // results to "true", boolean "true" is by default "1"

	// example (including an "if" statement)
	let num1 = 5
	if(num1 == 5) {

		/*alert("The number is 5.")*/
	}

/*	2. Inequality Operator (!=) with "if" and "if-else" statements */

	// ex. 1
	let turonvers = 9

	if(turonvers !=10) {

		console.log("You don't have 10 turonvers.")
	}

	// ex. 2
	let password = "admin1234"

	if(password != "admin123") {

		console.log("You've entered the wrong password.")
	}

	// ex. 3
	let isCompleted = true

	if(true) {
		console.log("Your package is completed.")
	} 
	if(!isCompleted) {
		console.log("Your package was not completed.")
	}

	// ex. 4
	let numberExample1 = 10

	if (!isNaN(numberExample1)) {
		console.log("Thank you for the appropriate input.")
	} else {
		console.log("Please input a number.")
	}

/*	3. Strict Equality Operator (===)
		- a strict comparison is only true if the operands are of the same type and the contents match.
		- Performs no type conversion.
*/
	
	console.log(1 === 1) // results to true
	console.log("1" === 1) // results to false, not the same data type


	// Examples with an "if-else" statement:

	// ex. 1
	let username = "admin"

	if(username === "admin") {

		console.log("Good morning, " + username + ".")
	}

	// ex. 2
	let numEx2 = 5

	if (typeof numEx2 === "number")

		console.log("The data-type is a number.")

	// Example with an "if-else statement"
	let batman = "superhero"

	if (batman === "superhero") {
		console.log("Batman is Gotham's Pride!")
	
	} else {
		console.log("Batman")
	}


/* LOGICAL OPERATORS (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Logical) */

/* 1. && (and) Logical Operator
	- the conditions on both the left and right must be met to run the code block.
*/
	
	// ex. 1
	let myFirstName = "John"
	let myLastName = "Angeles"

	if (myFirstName === "John" && myLastName === "Angeles") {
		console.log("Hello, " + myFirstName + " " + myLastName + ".")

	} else {
		console.log("Wrong Credentials.")
	}

	// ex. 2
	let numExample1 = 15

	if (numExample1%10 === 0 && numberExample1%3 === 0) {
		console.log("The number is divisible by both 10 and 3.")

	} else {
		console.log("The number is not divisible by 10 or 3.")
	}

/* 2. || (or) Logical Operator */

	// ex. 1
	let numberExample2 = 24

	if (numberExample2%8 === 0 || numberExample2%3 === 0) {
		console.log("The number is divisible by 8 or 3.")

	} else {
		console.log("The number is not divisible by 8 or 3.")
	}


/* CONDITIONAL TERNARY OPERATOR (https://developer.mozilla.org/en-US/docs/Web/JavaScript/Guide/Expressions_and_Operators#Conditional)
	- The Ternary Operator (?) is a shorthand way of writing an if/else statement
	- It takes 3 operands in the following format:
	- Condition ? (code to run if condition is true) : (code to run if condition is NOT true)
*/

	let numberExample3 = 24

	numberExample3%5 === 0 ? (console.log("It is divisible by 5."))

	: (console.log("It is not divisible by 5."))

	// The Ternary Operator shortens your code but also make it more difficult to read. Use with caution.
