/* JS REFRESHER (Pt. 3) */

	/*
	This page covers the ff:
		- Arrays
		- Array Mutator Methods
		- Array Accessor Methods
		- Array Iteration Methods
		- Multidimensional Arrays

	Folder Sources: wd004-s4-d1
	*/


/* ARRAYS
	- enables us to store a collection of data
	- arrays are created by using the [] notation, otherwise known as an "Array Literal".
*/

	let myArray = []
	let myArray2 = []
	console.log(myArray === [])

	let student1 = "Lorenzo"
	let student2 = "Mary"
	let student3 = "Sidney"
	let student4 = "Med"

	// An array of strings
	let studentArray = [student1,student2,student3,student4]
	console.log(studentArray) // returns as: "Lorenzo", "Mary", "Sidney", "Med"

	// An array of numbers
	let numArray = [2,4,6,7,8]
	console.log(numArray)
	// returns as: 2, 4, 6, 7, 8

	// Though not advisable, you can create an array with a mix of different
	// data types

	let stuff = [1, "apples", "oranges", undefined, 25, [],[]]
	console.log(stuff)

	/* Adding to an Array
			- we don't have to pre-populate an array upon declaration
			- we can add into an array using the following:
	*/

		let newArray = []
		console.log(newArray)

		// by passing in 0 as the index, we can add our first item:
		newArray[0] = "Pineapples"
		console.log(newArray[0])

		// arrays are zero-based, meaning that the first element/item is stored at position 0.
		newArray[1] = "Kiwi"
		console.log(newArray)

	/* Example of a Mutator Method: Push */

		newArray.push("Mangoes")
		// allows us to add an item into our array, by passing an argument through the method "push".
		// this will allow to add an item at the end of our array.

		console.log(newArray[2])
		console.log(newArray)
		
		// multiple entries are separated by commas		
		newArray.push("Oranges", "Apples", "Grapes")
		console.log(newArray)

	/* Reading from an Array */

		// new array
			// [0] Pineapples
			// [1] Kiwi
			// [2] Mangoes
			// [3] Oranges
			// [4] Apples
			// [5] Grapes

		console.log(newArray[5]) // displays "Grapes"
		console.log(newArray[2]) // displays "Mangoes"

		newArray[3] = "Coconut" // you can also update the items this way
		console.log(newArray[3]) // displays "Coconut"

	/* How do we know how many items are there in our array?
			- Arrays also have a .length property */

		console.log(newArray.length) // returns 6

	/* For Loops */

		for(let x = 0; x < newArray.length; x++) {
			console.log(newArray[x]) // this will list down all the items on the console
		}

		console.log(newArray[newArray.length-1])
		// this will show the last item in the array on the console which for this example is "Grapes"


/* MUTATOR METHODS
	- allow us to modify our array

	1. Push Method
		- allows us to add item to the last position of our array
		- the syntax is: arrayName.push(argument)
		- if we kept pushing and want to know the last item we added,
		  we can use: arrayName[arrayName.length-1] to access the last item in our array.
*/

/* 2. Pop Method
		- removes the last element and returns the item you deleted
		- syntax: arrayName.pop()
*/
	let morningChores = [

	"clean frontyard",
	"cook some eggs",
	"walk the dog"

	]

	morningChores.pop() // this deleted "walk the dog" from the array

	/*alert(morningChores.pop())*/
	// returns the item that was deleted which was "walk the dog"

	console.log(morningChores)
	// this will now only return "clean frontyard" and "cook some eggs"

/*	3. Reverse Method
		- reverses the order of the elements in the array
*/
	
	let myTasks = [

		"feed the baby",
		"wash the baby",
		"shush the baby"
	]

	myTasks.reverse()

	console.log(myTasks) // this will return as "shush the baby" as the first item shown, the order is reversed

/*	4. Shift Method
		- removes the first element in the array & returns what was removed
*/

	let todayTasks = [

		"pay the bills",
		"code something fantastic",
		"walk the dog"
	]

	console.log(todayTasks.shift()) // returns "pay the bills" (first item) & removes it from the array
	console.log(todayTasks) // returns "code something fantastic", "walk the dog"

/* 5. Sort Method
		- sorts the element of an array in ascending order
		- converts all items to string & orders them alphabetically
*/

	let dailyChores = [
		"clean the room",
		"wash the dishes",
		"bring the paper in"

	]

	dailyChores.sort()
	console.log(dailyChores) // returns "bring the paper in", "clean the room", "wash the dishes" in alphabetical order

	// What about if the elements are numbers?

		let numArraySort = [5,66,12,25,100]
		numArraySort.sort()
		console.log(numArraySort) // returns as 100, 12, 25, 5, 66

	// It's recommended to create an anonymous function that compares the numbers in the array.

	/* What is an anonymous function?
		- it is a function w/o  callable name and works when declared */

	numArraySort.sort(function(a,b){return a - b})
	console.log(numArraySort) // returns the smallest number first: 5, 12, 25, 66, 100

	numArraySort.sort(function(a,b){return b - a})
	console.log(numArraySort) // returns the biggest number first: 100, 66, 25, 12, 5

	// Here, we created a function to compare the numbers.

/* 6. Splice Method
		- allows you to simultaneously add and remove elements from an array in 1 command.
		- Syntax: arrayName.splice(index, how many, then elements you want to add or remove; ex. element1, element2, element3)
*/

	let avengers = [

		"Captain America",
		"Iron Man",
		"Thor",
		"The Hulk",
		"Black Widow",
		"Scarlet Witch"

	]
	console.log(avengers) // returns as "Captain America", "Iron Man", "Thor", "The Hulk", "Black Widow", "Scarlet Witch"

	/* Let's remove 1 element from our array on the 2nd position, and insert 2 strings in its place. */
	avengers.splice(1, 1, "Quicksilver", "Black Panther")
	console.log(avengers)
	// returns as "Captain America", "Quicksilver", "Black Panther", "Thor", "The Hulk", "Black Widow", "Scarlet Witch" / "Iron Man was removed"

	// the passed in elements is optional
	// we can use the method just to delete elements
	avengers.splice(3, 4)
	console.log(avengers) // removes "Thor", "The Hulk", "Black Widow", "Scarlet Witch" and keep "Captain America", "Quicksilver", "Black Panther"

/* 7. Unshift Method
		- adds one or more element to the beginning of the array
		- and returns the new length of the array
*/

	let teacherTasks = [

		"clean the blackboard",
		"prepare lesson plans",
		"create materials",
		"check papers"

	]

	let newNumTasks = teacherTasks.unshift("record grades", "change clock's battery")
	console.log("You now have " + newNumTasks + " tasks to complete.") // returns as: You now have 6 tasks to complete.
	console.log(teacherTasks)
	// returns as: "record grades", "change clock's battery", "clean the blackboard", "prepare lesson plans", "create materials", "check papers"


/* ACCESSOR METHODS */

	/* 1. Concat
		- combines two or more arrays into 1
		- this creates and returns a new array and leaves the original arrays untouched
	*/

		let basketSkills = ["Shooting", "Dribbling", "Passing"]
		let baseBallSkills = ["Batting", "Catching", "Pitching"]
		let volleySkills = ["Serving"]

		let sportsSkills = basketSkills.concat(baseBallSkills,volleySkills)

		console.log(sportsSkills) // returns as: "Shooting", "Dribbling", "Passing", "Batting", "Catching", "Pitching", "Serving"
		console.log(basketSkills) // returns the same: "Shooting", "Dribbling", "Passing"

	/* 2. Join
		- takes the values in an array and joins them into a string
		- performs a string operation on each item prior to joining a parameter can be passed in to specify
		  the character to separate the items
		- by default, a comma will be used instead
	*/

		let luckyNum = [45, 23, 7, 12, 5,]
		/*alert("The winning lottery numbers are " + luckyNum.join(", "))*/
		// returns as: The winning lottery numbers are 45, 23, 7, 12, 5 / ", " - adds a comma in between the #s

	/* 3. Slice
			- allows us to return the selected elements as a new array
			- syntax: arrayName.slice(start, end)
			- by default, the start is 0
	*/

		let myChores = [

			"wash the car",
			"clean the windows",
			"fix the light bulb",
			"throw the trash"
		]

		let toDoMonday = myChores.slice(0,2)
		let toDoTuesday = myChores.slice(0)
		console.log(toDoMonday) // returns as: "wash the car", "clean the windows"
		console.log(toDoTuesday) // returns as: "wash the car", "clean the windows", "fix the light bulb", "throw the trash"
		console.log(myChores) // returns as: "wash the car", "clean the windows", "fix the light bulb", "throw the trash" (unchanged)

		/* Mini Activity 
			- Use slice to create new arrays with only the functionalities the following devices can commonly use.
		*/

			let functionalities = [

				"call",
				"use sms",
				"play MP3",
				"use wifi",
				"use GPS",
				"watch movies",
				"browse the net"

			]

			let nokia3310 = functionalities.slice(0,2)
			let smartphone = functionalities.slice(0)
			let desktop = functionalities.slice(2)
			let tablet = functionalities.slice(0)
			console.log(nokia3310)
			console.log(smartphone)
			console.log(desktop)
			console.log(tablet)

	/* 4. ToString */

		let arrToString = [

			"Never",
			"gonna",
			"give",
			"you",
			"up"

		]

		let newArrayString = arrToString.toString()
		console.log(newArrayString) // returns as: Never,gonna,give,you,up

		let arrToString2 = [

			"Rick",
			"Ashley",
			25,
			29,
			[1, 2, 3,],
			{name: "Mike"}

		]

		let newArrayString2 = arrToString2.toString()
		console.log(newArrayString2)

	/* 5. indexOf (optional parameter)
		- finds the first iteration of an item in an array & returns its index
		- this is done using strict equality
		- syntax: arrayName.indexOf(searchElement, fromIndex)
	*/

		let names = [

			"John",
			"Joseph",
			"Marcus", // first iteration of Marcus from 0
			"Nathan",
			"Kenny",
			"Marcus", // first iteration of Marcus from 3
			"Jane",
			"April",
			"Jake",
			"Joseph"

		]

		console.log("The index of the first iteration of Marcus is " + names.indexOf("Marcus"))
		// returns as: The index of the first iteration of Marcus is 2

		console.log("The index of the first iteration of Marcus is " + names.indexOf("Marcus", 3))
		// returns as: The index of the first iteration of Marcus is 5

		/* Mini Activity */

		let scoreTally = [

			67, 78, 89, 90, 78, 45, 90, 91,
			77, 50, 89, 80, 81, 84, 84, 82

		]

		console.log("The index of the score 50 is " + scoreTally.indexOf(50))
		// returns as: The index of the score 50 is 9

		console.log("The index of the score 77 is " + scoreTally.indexOf(77))
		// returns as: The index of the score 77 is 8

		console.log("The index of the second iteration of 78 is " + scoreTally.indexOf(78, 4))
		// returns as: The index of the second iteration of 78 is 4

		console.log("The index of the second iteration of 90 is " + scoreTally.indexOf(90, 6))
		// returns as: The index of the second iteration of 90 is 6

	/* 6. lastIndexOf (optional parameter)
		- it will return the last iteration of the searched element
		- syntax: arrayName.lastIndexOf(searchElement, fromIndex)
	*/

		let fruitsBasket = [

			"Orange",
			"Apples",
			"Plum",
			"Pineapples",
			"Avocado",
			"Lemon",
			"Apples"

		]

		console.log("The index of the last iteration of Apples is " + fruitsBasket.lastIndexOf("Apples"))
		// returns as: The index of the last iteration of Apples is 6


/* ITERATION METHODS */

	/* 1. forEach
		- works similar to a loop, foreach takes an anonymous function as its argument and
		  runs the function for each of the items in the array.
		- syntax: arrayName.forEach(function(items){})	 
	*/

		let arr = [

			"Eugene",
			"Alfred",
			"Dennis",
			"Vincent"
		]

		/*for(let x = 0; x < arr.length; x++) {
			console.log(arr[x])

		}*/

		arr.forEach(function(name) {

			console.log(name)
			// returns as: Eugene, Alfred, Dennis, Vincent (vertically)
		
		})

		let numArr = [15,46,23,24,14,16,30,55,50,8,4]
		let evenSum = 0
		let oddSum = 0

			numArr.forEach(function(num){

				if(num%2 === 0){

					console.log(num + " is an even number.")
					// displays all even numbers
					evenSum += num // will sum up all even #s within the array
				
				} else {

					console.log(num + " is an odd number.")
					// displays all odd numbers
					oddSum += num // will sum up all odd #s within the array
				}

			})

		console.log("The sum of all the even numbers in the array is: " + evenSum)
		console.log("The sum of all the odd numbers in the array is: " + oddSum)

	/* 2. Map()
	-	similar to forEach, but it always returns a new array after using the anonymous function
	*/

	// Make another array that will contain the squares of each element in our array
	
		let numArr2 = [1,2,3,4,5]

		let squaredNumArr2 = numArr2.map(function(num){

			return (num*num)

		})

		console.log(squaredNumArr2)

	/* Mini Activity 
		- use map to create an array of ["john", "paul", "george", "ringo"]
		- the following array should display ["I am a fan of "]
	*/

		let beatles = ["John", "Paul", "George", "Ringo"]

		let beatlesBand = beatles.map(function(name){

			return "I am a fan of " + name

		})

		console.log(beatlesBand)

	/* 3. Every
		- used to apply a set of criteria to validate the data in an array
		- returns a boolean (true or false)
		- if 1 element is false, the entire array will be false
	*/

		let arr2 = [1,2,3,4,5];
		let isValid = arr2.every(function(num){

			return (num < 10)

		})

	/*console.log(isValid)*/
	// returns as true because all #s in the array are less than 10. It will be false if the # is higher than 10.

	/* 4. Some
			- used to check if one or more items in an array will pass a given test
			- if some of the elements are true, the rest of the elements will be true
	*/	
	
		let arr3 = [1,2,3,4,5,100];

		let resultOfSum = arr3.some(function(num){

			return (num < 3);

		})

		console.log(resultOfSum)

	/* 5. Filter
			- creates a new array out of items that pass the given criteria
			- returns an array
			- this can also be used to remove just 1 item from an array
	*/

		let arr4 = [1,2,3,4,5,6,7,8,9];

		let filtered = arr4.filter(function(num){

			return (num < 5)

		})

		console.log(filtered)

	/* 6. Reduce
			- performs a Mathematical operation on an array
			  and reduce it to a single value
	*/

		let arr5 = [1,2,3,4,5]

		let total = arr5.reduce(function(previous,current){

			return previous + current

		})

		console.log(total)
		// returns as: 15 (it added all #s in the array)

		// 1st iteration
		total = 0
		previous = 0
		current = 1 // total is 1

		// 2nd iteration
		total = 1
		previous = total
		current = 2 // total is 3

		// 3rd iteration
		total = 3
		previous = total
		current = 3 // total is 6

		// 4th iteration
		total = 6
		previous = total
		current = 4 // total is 10

		// 5th iteration
		total = 10
		previous = total
		current = 5 // total is 15

	/* 7. Find
			- returns the first item that passed the condition
	*/

		let friends = ["Karen", "Med", "Jhon"]

		let result = friends.find(function(friend){

			return friend === "Med"

		})

		console.log(result)


/* MULTIDIMENSIONAL ARRAYS
	- array within an array/nested array
	- [string, number, boolean, []]
*/

	let nestedArray = [

		["Alex", 16, "instructor"],
		["Bryan", 18, "basketball player"]

	];

	// display 1
	/*console.log(nestedArray[0][0])
	console.log(nestedArray[0][0] + "is" + nestedArray[0][1] + "years old" + nestedArray[0][2])
	console.log(nestedArray[1][0] + "is" + nestedArray[1][1] + "years old" + nestedArray[1][2])*/

	// display 2
	/*nestedArray.forEach(function(profile){

		console.log(profile[0] + " is " + profile[1] + " years old " + profile[2])*/

	// display 3
	/*nestedArray.forEach(function(profile){

		console.log(profile[0])
		console.log(profile[1])
		console.log(profile[2])

	})*/

	// display 4
	/*nestedArray.forEach(function(profile){

		for(let x = 0; x < profile.length; x++)
			console.log(profile[x])
	})*/

	// display 5
	nestedArray.forEach(function(person){
		person.forEach(function(detail){
			console.log(detail)
		})
	})
