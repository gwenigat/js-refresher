/* JS REFRESHER (Pt. 2) */

	/*
	This page covers the ff:
		- Functions
		- Scope
		- Hoisting
		- Using If-Else Statements in Functions
		- Nesting If-Else Statements
		- Else-If Statements
		- Switch Statements
		- Loops > While Loops
		- Infinite Loops
		- Do..While Loops
		- For Loops
		- Nested Loops

	Folder Sources: wd004-s1-d1, s2-d1, s3-d1
	*/


/* FUNCTIONS
	- A JavaScript function is a block of code designed to perform a particular task. It is executed when "something" invokes it (calls it).
	- Function names can contain letters, digits, underscores, and dollar signs (same rules as variables).
	- Function parameters are listed inside the parentheses () in the function definition. The parentheses may include parameter names
	  separated by commas: (parameter1, parameter2, parameter3)
	- The code to be executed by the function is placed inside 2 curly brackets: {}
	- Function arguments are the values received by the function when it is invoked.
	- Inside the function, the arguments (the parameters) behave as local variables.

	Syntax:
	function functionName(parameter) {
		
		code to be executed

	}

	functionName(argument)
*/

	function saySomething () {
	    /*alert("Hello!")*/
	}
	saySomething();
	// the use of () is for passing the values as arguments into the function

	function saySomethingBack(msg) {
		/*alert("Hi!")*/
	}
	saySomethingBack();

	function add(numero1, numero2) {
		return numero1 + numero2
	}
	console.log(add(2,3)) // results to 5

	// Mini Activity: Dog Years
	function dogYears(human) {
		return human * 7
	}
	console.log(dogYears(2)) // results to 14

/* SCOPE
	- Variables can exist in either 1 of 2 scopes:
		- Global Scope: not declared within a function, available everywhere.
		- Local Scope: declared within a function, only available within.
		- A function can access global variables but not local variables.
*/

	// ex. 1
	let governor = "Ynares" /*global*/
	function fNames(name) {

		let govFirstName = "Mike" /*local*/
		console.log(`His name is ${govFirstName} ${governor}.`)
	}
	fNames()

	  console.log(governor) // returns as: Ynares
	//console.log(govFirstName) - returns as: govFirstName is not defined

	function rizalGov() {
		let governorName = "Jun"
		console.log(`Rizal's Governor is ${governorName} ${governor}.`) // returns as: Rizal's Governor is Jun Ynares.
	}
	rizalGov()

	// ex. 2
	function fullName100() {
		let firstName100 = "Juan"

		function alertFullName10() {
			let lastName100 = "Dela Cruz"
			alert("Full Name: " + firstName100 + " " + lastName100)
		}
		/*alertFullName10()*/
	}
	fullName100();

	// ex. 3
	let a = "a"

	function levelb(){
		let b = "b"
		function levelc(){
			let c = "c"
			function leveld(){
				let d = "d"
				console.log("level d", a, b, c, d)
			}
			leveld()
			console.log("level c", a, b, c)
		}
		levelc()
		console.log("level b", a, b)
	}
	levelb();

/* HOISTING
	- a JS mechanism where functions and variables declared are moved to the top of their scope before code execution.
*/

	console.log(herName) // undefined
	var herName = "Emma"

	function nameHer(){
		var herName = "Audrey"
		console.log(herName) // Audrey
	}
	nameHer()

/* Mini-Activity: Fahrenheit to Celsius Converter */

	function fToCConverter(fahrenheit) {

		var fTemp = fahrenheit
		var fToCel = (fTemp - 32) * 5 / 9
		var msg = fTemp + ' degrees Fahrenheit is ' + fToCel + ' degrees Celsius.'
			console.log(msg)
	}

	fToCConverter(212)


/* USING IF-ELSE STATEMENTS IN FUNCTIONS */

	let password1 = "adminadmin123"
	let password2 = "adminadmin123"
	let username01 = "admin"

	function adminLogin(user,pw1,pw2) {

		if (user === username01) {

			console.log("You've entered the correct username.")

				if (pw1 === password1) {

					console.log("Thank you, please confirm your password.")
					
					if (pw2 === password2) {

						console.log("Thank you for logging in.")
					
					} else {

						console.log("Please input the correct password.")
					}

				} else {

					console.log("That is the wrong password.")
				}

		} else {

			console.log("You've entered the wrong username.")

		}
	}

	adminLogin("admin","adminadmin123","adminadmin123")


/* NESTING IF-ELSE STATEMENTS */

	let numEx3 = 25

	if (!isNaN(numEx3)) {

		console.log("This is a number.")

		if (numEx3 == 25) {
			
			console.log("The number is 25.")

			if (numEx3%5 === 0) {

				console.log("The number is divisible by 5.")
			}

		} else {
			
			console.log("The number is not 25.")
		}

	} else {

		console.log("This is not a number.")
	}


/* ELSE-IF STATEMENTS */

	let dayToday = "Wednesday"

	if (dayToday === "Monday") {

		// this code will run if the 1st condition was met.
		console.log("I should wear a green shirt.")

		} 	else if (dayToday === "Wednesday") {

			// this code will run if the 1st condition was not met
			// but the 2nd condition was met.
			console.log("I should wear purple today.")

		} 	else if (dayToday === "Friday") {

			// this code will run if the 1st condition was not met
			// but the 2nd condition was met.
			console.log("I should wear blue today.")

	} else {

		// this code will run if both conditions were not met.
		console.log("I should wear a pink shirt today.")
	}


/* SWITCH STATEMENTS
	- We can string together if else statements to form logical decision tree.
*/

	// Simulate a dice roll:
	let diceRoll = Math.floor(Math.random()*6) + 1;

	// Math.floor in JS is a Math object method which rounds down a passed in number.
	// Math.random generates a random # between 0 & 1 (inclusive of zero)

	console.log(diceRoll)

	if(diceRoll === 4){

		console.log("You rolled a four!")

	} else if (diceRoll === 5) {
		console.log("You rolled a five!")

	} else if (diceRoll === 6) {
		console.log("You rolled a six!")

	} else {
		console.log("You rolled a number less than 4.")
	}

	// Switch can be used instead to shorthand. It automatically uses strict equality operator (===)

switch(diceRoll) {

	case "4":
		console.log("You rolled a 4!")
	break;

	case "5":
		console.log("You rolled a 5!")
	break;

	case "6":
		console.log("You rolled a 6!")
	break

	default:
		console.log("You rolled a number less than 4.")
	break;
}

	/* Mini-Activity
		- Dwindle the enemy HP to 0 with attacks using switch.
		- Hint: Each time an attack is made, the enemy HP's value is saved to itself.
	*/

	let enemyHP = 1250;
	let attack = 200;

	function attackInput(atkString) {

		switch(atkString) {

			case "hadouken":
				enemyHP -= attack
				console.log("You used hadouken!")
			break;

			case "kamehameha":
				enemyHP -= attack
				console.log("You used kamehameha!")
			break;

			default:
				console.log("Invalid attack.")
			break;
		}
	}

	attackInput("hadouken");
	console.log(enemyHP)


/* LOOPS
	- used to repeat pieces of code over and over according to certain conditions.
	
	   WHILE LOOPS
	    - runs a block of code while the given condition is true

		syntax:
		while(condition){
			do something
		}
*/

	let tasks = 7;
	// pass in the condition for the while loop
	while(tasks > 0) {
		// the below code will be repeated as long as tasks > 0
		console.log("There are " + tasks + " tasks left.");
		// decrement tasks every iteration
	tasks--;
	}

	let count = 1

	while(count <= 10) {

		console.log("The number is " + count)

		count++;
	}

	console.log(count)

	/* Mini-activity
		- Print the numbers (-10 through 19) on your console
	*/
		let counter2 = -10

		while(counter2 < 20) {
		
		console.log(counter2)
		counter2++
		}

		console.log(counter2)

	// Print all odd numbers between 300 and 333.

		let counter3 = 300;

		while(counter3 < 333) {
			if(counter3%2 !== 0) {
				console.log(counter3)
			}

			counter3++
		}

	// Print all even numbers from 10 through 40.

		let counter4 = 10;

		while(counter4 < 42) {
			if(counter4%2 === 0) {
				console.log(counter4)
			}

			counter4++
		}


/* INFINITE LOOPS
	- it is important that the given condition in a while loop
	- will be met at some point
	- if not, the loop will be infinite; crashing your program
*/


/* DO..WHILE LOOPS
	
	Similar to a while loop, only difference being the condition is given AFTER the code block
	
	Syntax:

		do {
		  do something
		} while(condition)

	This way, the block of code will always run AT LEAST ONCE, regardless of the condition
	being true or not. The tasks while loop example can be written as a do..while with the same results.	
*/
	
	let newTasks = 5;

	do {
		console.log("There are " + newTasks + " tasks left.");
		newTasks--;
	} while (newTasks > 0)

/* The above example guarantees that the code block will be run at least once no matter
   what value is assigned to the variable tasks.
*/

	let newTasks2 = 5;

		while(newTasks2 > 0) {

			console.log(`${newTasks} left.`)
			newTasks2--
		}


/* FOR LOOPS

	Syntax:

	for(initilization; condition; after) {
		
		do something
	}
	
	The initialization is run BEFORE the loop starts. Usually used to initialize variables to be used in the loop.

	The condition has to be satisfied on each iteration for the code block to run. The loop will stop once the condition is no longer met.

	The after code specifies what to do after each iteration of the loop
	typically used to increment / decrement a counter of some sort.
*/

	// We can write the above tasks while loop as a for loop:

		for(let forTasks = 5; forTasks > 0; forTasks--) {
			console.log("There are " + forTasks + " tasks left.");
		}

		for(let countdown = 10; countdown >= 0; countdown-- ) {
			console.log(countdown)
		}

	// Mini-Activity: Multiplicaton Table

	let num = 5
	for(let x = 1; x <= 10; x++) {
		console.log(num * x)
	}

	let num2 = 7
		for(let x = 1; x <= 10; x++) {
			console.log(num2 * x)
		}

	let num3 = 9
		for(let x = 1; x <= 10; x++) {
			console.log(num3 * x)
		}


/* NESTED LOOPS
	- you can place a loop inside another loop to create a nested loop
	- the inner loop will run all the way through before the outer loop iterates
*/

	for(let x = 1; x <= 3; x++) {
		
		console.log("Hello from the outer loop, this is no. " + x)

		for(let y = 1; y <= 3; y++) {

			console.log("Hello from the inner loop, this is no. " + y)
		}

	}

	for(let x = 1; x <= 3; x++)

		for(let y = 1; y <= 3; y++)

			console.log(x + y)

	/*
		1st iteration of outer loop the x (outer loop) = 1
		x+y 1+1 = 2, 1st iteration of inner loop
		x+y 1+2 = 3, 2nd iteration of inner loop
		x+y 1+3 = 4, 3rd iteration of inner loop
		2nd iteration of outer loop the x= 2
		x+y 2+1 = 3, 1st of inner loop during 2nd iteration (outer loop)
		x+y 2+2 = 4, 2nd of inner loop during 2nd iteration (outer loop)
		x+y 2+3 = 5, 3nd of inner loop during 2nd iteration (outer loop)
		3rd iteration of outer loop the x= 3
		x+y 3+1 = 4, 1st of inner loop during 3rd iteration (outer loop)
		x+y 3+2 = 5, 2nd of inner loop during 3rd iteration (outer loop)
		x+y 3+3 = 6, 3rd of inner loop during 3rd iteration (outer loop)
	*/

	/* 
		Mini Activity: Nested Loops
			Using nested loops, create a multiplication from 1 through 10.
			Message on the console should be "1x1 is equal to 1"
	*/

		 for(let x = 1; x <= 10; x++) {

		 	for(let y = 1; y <= 10; y++) {

		 	console.log(x * y)
		 	
		 	}
		 }
