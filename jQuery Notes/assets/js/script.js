
/* In JS, how do we get our elements (node)? */
	
	/*let variable1 = document.getElementById('flash')
	
	let variable2 = document.querySelector("#batman")

	console.log(variable2)*/ // Bruce Wayne


/* jQuery Syntax:
	- $("selector").method()
*/

	console.log($("#batman"))
	// returns as: [li#batman.hero]

	// Saving to a variable:
	let heading = $("#heading")
	console.log($(heading))
	// returns as: [h1#heading]

	/*$("#batman").hide()*/
	// this will hide Bruce Wayne


/* Styling
	- Syntax: $("selector").css("property", "value")
*/

	/*$("h1").css("color", "green")*/
	// h1 changed to color green

/* Applying Multiple Styles
	- First, create a variable and assign an object that contains the styles then pass the variable in the CSS method
*/

	// ex.
	let h1Style = {

		color: "green",
		border: "2px solid black",
		backgroundColor: "skyblue"

	}
	$("h1").css(h1Style)

	/*$("ul").css(h1Style)*/


/* jQuery Methods */

	// 1) $(selector).text() Method
	// 	- gets the text content of the element.

		console.log($("h1").text("Hello, Gwen!"))
		// h1 text was changed to "Hello, Gwen!"

	// 2) $(selector).html() Method
	//	- similar to innerHTML, returns all children of the element as a string.

		console.log($("ul").html())
		// check console

		/*$("ul").html("<li>Robin</li><li>Jean Grey</li>")*/
		// this will replace/overwrite the <li>s in index.html

	// 3) $(selector).attr() Method
	//	- syntax: $(selector).attr("attribute", "val")
	//	- gets the value of an attribute for the first element.

		/*console.log($("li").attr("id"))*/
		// returns as: undefined, because the new <li>s don't have IDs

		/*$("li").attr("id", "youngJustice")*/
		// check Inspect > Elements:
		// <li id="youngJustice">Robin</li>
		// <li id="youngJustice">Jean Grey</li>
		/*console.log($("li").attr("id"))*/
		// returns as: youngJustice

		/*$("li").attr({ id: "teenTitan", class: "sideKicks"})*/
		// this will overwrite the previous IDs and its values then we added a class

	// 4) .val() Method
	// 	- gets the value of the selected element.

		console.log($("#addHero").val())
		// this will return as blank on the console
		// type something in the input field then type $("#addHero").val() in the console 

		let input = $("#addHero")
		// type something in the input field then type input.val() in the console

		input.val("Zuitt")
		// "Zuitt" will show as the value in the input field on the page
		// this says that we can also update the value of our input by passing an argument through the .val() method


/* Manipulating Classes */
	
	// 1) .addClass()
	// 	- this is almost the same as classList.add() method

		$("#superman").addClass("correct")
		// added a class called: correct
		// this will change the color of Clark Kent on the page to green (also check CSS)

	// 2) .removeClass()

		$("#superman").removeClass("correct")
		// removed the class called: correct, therefore this will remove the color green of Clark Kent on the page and turns it to the default color black

	// 3) .toggleClass()

		$("#batman").toggleClass("correct") // green Bruce Wayne
		$("#batman").toggleClass("correct") // black Bruce Wayne


/* jQuery Events */

		// 1) CLICK EVENT
			// syntax: pass an anonymous function as an argument
				/*$("#heading").click(function() {
					alert("The heading has been clicked.")*/
					// click the heading
				/*})*/

				/*$("li").click(function() {*/
					/*alert("This list item has been clicked.")*/
					// click the <li>s
				/*})*/

				/*$("li").click(function() {*/

					let text = $(this).text()
					// "this" keyword refers to the element the function is attached to, which for this example is the <li> element.

					console.log(text)
					// click one <li> and the console will return the text content of that <li>.

					/*alert(text)*/
					// click one <li> and the alert will show the text content of that <li>.
				/*})*/

		// 2) KEYPRESS EVENT

			// ex. 1
			$("#addHero").keypress(function(){
				/*alert("You pressed a key.")*/
				// select the input field then press a key
			})

			// ex. 2
			$("#addHero").keypress(function(event){
				console.log(event.key)
				// select the input field and press a key, it will show on the console
			})

			// ex. 3
			/*$("#addHero").keypress(function(event){
				let input = $(this).val()
				console.log(input)
				// shows what you type in the input field on the console
			})*/

			// ex. 4
			/*$("#addHero").keypress(function(event){
				let input = $(this).val()

				if(event.key === "Enter") {

				console.log(input)
				// Enter will only show on the console when you press Enter
				}
			})*/

			// ex. 5
			$("#addHero").keypress(function(event){
				let input = $(this).val()

				if(event.key === "Enter") {

				$("#roster").append("<li>" + input + "</li>")
				// works like appendChild in vanilla JS
				// type something in the input field and press Enter, the typed word will be added to the <li>s
				}
			})

		// 3) ON EVENT / .on()
			// similar to addEventListener, allows to specify the event
				
				// a. mouseenter

					$("li").on("mouseenter", function(){

						$(this).toggleClass("done")
						// mouseover the <li>s and it will put a grey strike-through on the <li>s (check .done in CSS)
					})

				// b. mouseleave

					$("li").on("mouseleave", function(){

						$(this).toggleClass("done")
						// mouseover the <li>s, it will put a grey strike-through but removes it when the mouseover leaves that <li>
					})


/* jQuery Effects */

	// 1. fadeOut

		// ex. 1
		/*$("li").on("click", function(){

			$(this).fadeOut(1000)*/ // 1 millisecond
			// click on the <li>s to see the effect
		/*})*/

		// ex. 2
		/*$("li").on("click", function(){

			$(this).fadeOut(1000, function(){

				$(this).remove()*/
				// this will remove the <li> from the list
				// check Inspect > Elements and click on the <li>s to see the effect
		/*	})
		})*/

	// 2. slideToggle

		$("li").on("click", function(){

			$("#heading").slideToggle(1000)
			// click on an <li> and the heading will slide up
		})