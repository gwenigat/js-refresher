/* JS REFRESHER (Pt. 4) */

	/*
	This page covers the ff:
		- Objects
		- Accessing the Object
		- Adding Properties to an Object
		- Objects in an Array
		- Using Array Method

	Folder Sources: wd004-s5-d1
	*/


/* JS Objects 
	- in JS, objects contain a set of information
	- in creating an object, use {}
*/

	let person = {

	/*key*/	firstName: "John", /*value*/
		lastName: "Wick",
		car: "Mustang",
		plateNumber: "ABC123",
		age: 32,
		isDead: false,
		hobbies: ["eat", "sleep", "code"]
	}

	console.log(person) // see console

/* ACCESSING THE OBJECT
	- use dot notation
	- object.key
		- key-value pair syntax
*/

/* ADDING PROPERTIES TO AN OBJECT */
	
	// using bracket notation
	person["gender"] = "male"

	// using dot notation
	person.height = 178

	// nested objects

		// ex. 1
		let person1 = {
			name: {
				first: "Juan",
				last: "Jule"
			}
		}
		console.log(person1) // see console

		// ex. 2
		let jaguar = {

			// properties
			color: "red",
			maxSpeed: 400,

			// method
			start : function(speed){
				console.log(`Running at the speed of ${maxSpeed} km/h`)
			},

			describe: function(){
				console.log(this)
				console.log(`Jaguar is color ${this.color}.`)
			}

		}

		// ex. 3
		let Person1 = {
			name: "Alex",
			description: "Pogi"
		}

		// ex. 4
		let Person2 = {

			name: "Leo",
			description: "Brave"
		}

/* OBJECTS IN AN ARRAY (Review) */

let koponanNiRecca = [

	{
		name: "Recca",
		age: 17,
		power: "Flame Dragon"
	},

	{
		name: "Max",
		age: 17,
		power: "Ring of Saturn"
	},

	{
		name: "Airra",
		age: 17,
		power: "Fuujin"
	},

	{
		name: "Lorkhan",
		age: 11,
		power: "Kogan Anki"
	}

]

/* Accessing properties of objects in an array */

	console.log(koponanNiRecca[2].power)

/* Using Array Method */

	koponanNiRecca.forEach(function(object){

		console.log(object.name)

	})

	function ageGroupChecker() {

		koponanNiRecca.forEach(function(object){

			if(object.age <= 11) {

				console.log(object.name + " is an elem student.")
			
			} else {

				console.log(object.name + " is a high school student.")
			}

		})
	}

	ageGroupChecker()
